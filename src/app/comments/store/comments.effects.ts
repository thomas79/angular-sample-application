import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map, switchMap, catchError, tap } from 'rxjs/operators';
import { Action } from '@ngrx/store';

import * as commentsActions from './comments.actions';
import { CommentsService } from '../services/comments.service';


@Injectable()
export class CommentsEffects {

  constructor(
    private actions$: Actions,
    private comments: CommentsService
  ) { }

  @Effect()
  load$: Observable<Action> = this.actions$.pipe(
    ofType(commentsActions.Types.LOAD),
    switchMap(() => this.comments.getAll().pipe(
      map(comments => new commentsActions.LoadSuccess(comments)),
      catchError(() => of(new commentsActions.LoadFail()))
    ))
  );
}

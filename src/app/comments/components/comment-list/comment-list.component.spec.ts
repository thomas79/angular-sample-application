import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { CommentListComponent } from './comment-list.component';
import { Wrapper } from 'src/utils/wrapper';

const DEFAULT_POST_ID = 1;
const UPDATED_POST_ID = 100;

describe('[Component] CommentList', () => {
  let component: CommentListComponent;
  let fixture: ComponentFixture<CommentListComponent>;
  let wrapper: Wrapper<CommentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentListComponent ],
      imports: [ FormsModule ],
    })
    .compileComponents();

    fixture   = TestBed.createComponent(CommentListComponent);
    component = fixture.componentInstance;
    wrapper   = new Wrapper(fixture);
  }));

  describe('Initialisation', () => {
    it('Initialises successfully', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Selecting a post ID', () => {
    beforeEach(() => {
      spyOn(component.selectPostId, 'emit');
    });

    it('Emits an event with the current post ID when the submit button is clicked', () => {
      wrapper.simulate('click', 'button');

      expect(component.selectPostId.emit)
        .toHaveBeenCalledWith(DEFAULT_POST_ID);
    });

    it('Emits new events with any updated post IDs on subsequent button clicks', () => {
      component.postId = UPDATED_POST_ID;
      wrapper.simulate('click', 'button');

      expect(component.selectPostId.emit)
        .toHaveBeenCalledWith(UPDATED_POST_ID);
    });
  });
});

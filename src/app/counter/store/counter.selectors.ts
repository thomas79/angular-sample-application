import { CounterState } from './counter.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const getCounterState = createFeatureSelector<CounterState>('counter');

export const getCurrentValue = createSelector(
  getCounterState,
  (state: CounterState) => state.currentValue
);

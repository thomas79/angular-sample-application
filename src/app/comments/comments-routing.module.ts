import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommentListContainer } from './components/comment-list/comment-list.container';

const routes: Routes = [
  {
    path: '',
    component: CommentListContainer
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommentsRoutingModule { }

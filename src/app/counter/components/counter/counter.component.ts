import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-counter-cmp',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent {

  @Input() currentValue: Observable<number>;
  @Output() increment = new EventEmitter();
  @Output() decrement = new EventEmitter();
  @Output() reset = new EventEmitter();

  onIncrementClick(): void {
    this.increment.emit();
  }

  onDecrementClick(): void {
    this.decrement.emit();
  }

  onResetClick(): void {
    this.reset.emit();
  }

}

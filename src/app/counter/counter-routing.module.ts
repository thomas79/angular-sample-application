import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounterContainer } from './components/counter/counter.container';

const routes: Routes = [
  {
    path: '',
    component: CounterContainer
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CounterRoutingModule { }

# Angular + ngrx Demo

This is a simple project demonstration how Angular works with ngrx for state management.

The app includes the following:
- Splitting discrete features into modules
- Lazily loading modules using Angular's router and webpack code splitting
- Using ngrx to separate the application's UI and state
- How application state interacts with asynchronous data (HTTP requests)
- Unit test examples for the application's state and user interface

There are two modules within the application: A counter and a comment list.

The counter can be increased and decreased, and may be useful as a reference for how ngrx actions, reducers and selectors work.

The comments list fetchs dummy data from an external source, and renders it onto the page. This may be useful as a reference for how to perform HTTP and other
async requests using ngrx effects and Angular services.

The components in both modules are wired up to ngrx using selectors. This may serve as an example of how to connect UI to application state.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

import { ActionReducer } from '@ngrx/store';

import { Actions, Types } from './counter.actions';

export interface CounterState {
  currentValue: number;
}

export const initialState: CounterState = {
  currentValue: 0
};

export const reducer: ActionReducer<CounterState> = (state: CounterState = initialState, action: Actions) => {
  switch (action.type) {
    case Types.INCREMENT:
      return increment(state, action.payload);

    case Types.DECREMENT:
      return decrement(state, action.payload);

    case Types.RESET:
      return initialState;

    default:
      return state;
  }
};

const increment = (state: CounterState, payload: number) => ({ ...state, currentValue: state.currentValue + payload });
const decrement = (state: CounterState, payload: number) => ({ ...state, currentValue: state.currentValue - payload });

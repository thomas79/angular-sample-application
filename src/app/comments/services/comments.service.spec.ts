import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { CommentsService } from './comments.service';
import { Comment } from '../interfaces/comments.interfaces';
import { of } from 'rxjs';

const COMMENTS: Comment[] = [
  {
    id: 1,
    postId: 10,
    body: 'hello I am a test comment',
    name: 'cool dude',
    email: 'dude@cool.com'
  },
  {
    id: 2,
    postId: 10,
    body: 'hello I am another test comment',
    name: 'cooler dude',
    email: 'dude@cooler.com'
  }
];

describe('[Service] Comments', () => {
  let service: CommentsService;
  let http: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    http = jasmine.createSpyObj('http', ['get']);

    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: http }]
    });

    service = TestBed.get(CommentsService);
  });

  describe('Initialisation', () => {
    it('Initialises successfully', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('#getAll', () => {
    it('Returns all comments from the backend', () => {
      http.get.and.returnValue(of(COMMENTS));

      service.getAll().subscribe(res => {
        expect(res).toBe(COMMENTS);
      });
    });
  });
});

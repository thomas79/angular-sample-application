import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Comment } from '../interfaces/comments.interfaces';

const URL = 'https://jsonplaceholder.typicode.com/comments';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private http: HttpClient) { }

  public getAll(): Observable<Comment[]> {
    return this.http.get<Comment[]>(URL);
  }
}

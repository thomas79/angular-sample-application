import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { reducer } from './store/comments.reducer';
import { CommentsEffects } from './store/comments.effects';
import { CommentsRoutingModule } from './comments-routing.module';

import { CommentPostComponent } from './components/comment-post/comment-post.component';
import { CommentListComponent } from './components/comment-list/comment-list.component';
import { CommentListContainer } from './components/comment-list/comment-list.container';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forFeature('comments', reducer),
    EffectsModule.forFeature([CommentsEffects]),
    CommentsRoutingModule
  ],
  declarations: [CommentPostComponent, CommentListComponent, CommentListContainer],
  exports: [CommentPostComponent, CommentListComponent, CommentListContainer]
})
export class CommentsModule { }

import * as fromCounter from './counter.reducer';
import * as Actions from './counter.actions';

const { initialState, reducer } = fromCounter;
const CUSTOM_INCREMENT = 10;
const DEFAULT_INCREMENT = 1;

describe('[Reducer] Counter', () => {
  let result: fromCounter.CounterState;
  let testState: fromCounter.CounterState;

  describe('By default', () => {
    it('Returns the existing state', () => {
      expect(reducer(initialState, {} as any)).toEqual(initialState);
    });
  });

  describe('INCREMENT', () => {
    it('Increases the counter by 1 by default', () => {
      result = reducer(initialState, new Actions.Increment());
      expect(result.currentValue).toBe(initialState.currentValue + DEFAULT_INCREMENT);
    });

    it('Increases the counter by a custom amount if specified', () => {
      result = reducer(initialState, new Actions.Increment(CUSTOM_INCREMENT));
      expect(result.currentValue).toBe(initialState.currentValue + CUSTOM_INCREMENT);
    });
  });

  describe('DECREMENT', () => {
    it('Decreases the counter by 1 by default', () => {
      result = reducer(initialState, new Actions.Decrement());
      expect(result.currentValue).toBe(initialState.currentValue - DEFAULT_INCREMENT);
    });

    it('Decreases the counter by a custom amount if specified', () => {
      result = reducer(initialState, new Actions.Decrement(CUSTOM_INCREMENT));
      expect(result.currentValue).toBe(initialState.currentValue - CUSTOM_INCREMENT);
    });
  });

  describe('RESET', () => {
    beforeEach(() => {
      testState = {
        currentValue: 100
      };
    });

    it('Resets the current counter value to its initial value', () => {
      result = reducer(testState, new Actions.Reset());
      expect(result.currentValue).toBe(initialState.currentValue);
    });
  });
});

import { ComponentFixture } from '@angular/core/testing';

/**
 * A test wrapper for Angular components.
 */
export class Wrapper<T> {
  /**
   * Creates a new Wrapper
   * @param fixture The component fixture, provided by Angular's TestBed.
   */
  constructor(private fixture: ComponentFixture<T>) {}

  /**
   * Finds an element within the component's template matching a CSS selector.
   * @param cssSelector The CSS selector to search the component by.
   * @returns the HTML element found.
   */
  public find(cssSelector: string): HTMLElement {
    return this.fixture.nativeElement.querySelector(cssSelector);
  }

  /**
   * Finds *all* elements within the component's template matching a CSS selector.
   * @param cssSelector The CSS selector to search the component by.
   * @returns the HTML elements found.
   */
  public findAll(cssSelector: string): HTMLElement[] {
    return new Array().slice.call(
      this.fixture.nativeElement.querySelectorAll(cssSelector)
    );
  }

  /**
   * Simulates clicking a HTML element within the component.
   * @param cssSelector The CSS selector to search the component by.
   */
  public simulate(event: string, cssSelector: string): void {
    this.find(cssSelector).dispatchEvent(new Event(`${event}`));
  }
}

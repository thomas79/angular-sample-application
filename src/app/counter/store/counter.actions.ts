import { Action } from '@ngrx/store';

export enum Types {
  INCREMENT = '[Counter] Increment',
  DECREMENT = '[Counter] Decrement',
  RESET = '[Counter] Reset'
}

export class Increment implements Action {
  readonly type = Types.INCREMENT;

  constructor(public payload: number = 1) {}
}

export class Decrement implements Action {
  readonly type = Types.DECREMENT;

  constructor(public payload: number = 1) {}
}

export class Reset implements Action {
  readonly type = Types.RESET;
}

export type Actions = Increment
  | Decrement
  | Reset;

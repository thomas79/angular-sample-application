import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { getCurrentValue } from '../../store/counter.selectors';
import * as Actions from '../../store/counter.actions';
import { CounterState } from '../../store/counter.reducer';

@Component({
  selector: 'app-counter',
  template: `
    <app-counter-cmp
      [currentValue]=currentValue$
      (increment)=handleIncrement()
      (decrement)=handleDecrement()
      (reset)=handleReset()
    >
    </app-counter-cmp>
  `
})
export class CounterContainer implements OnInit {

  public currentValue$: Observable<number>;

  constructor(
    private store: Store<CounterState>
  ) { }

  ngOnInit() {
    this.currentValue$ = this.store.pipe(select(getCurrentValue));
  }

  handleIncrement(): void {
    this.store.dispatch(new Actions.Increment());
  }

  handleDecrement(): void {
    this.store.dispatch(new Actions.Decrement());
  }

  handleReset(): void {
    this.store.dispatch(new Actions.Reset());
  }

}

import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';

import { CommentsState } from '@app/comments/store/comments.reducer';
import { Comment } from '@app/comments/interfaces/comments.interfaces';
import { getOne } from '@app/comments/store/comments.selectors';

import * as Actions from '../../store/comments.actions';

@Component({
  selector: 'app-comment-list',
  template: `
    <app-comment-list-cmp
      (selectPostId)=handleChangeId($event)
    >
    </app-comment-list-cmp>
    <app-comment-post
      [comments]=filteredComments$
    >
    </app-comment-post>
    `
})
export class CommentListContainer {

  public filteredComments$: Observable<Comment[]>;

  constructor(
    private store: Store<CommentsState>
  ) {
    this.store.dispatch(new Actions.Load());

    this.filteredComments$ = this.store.pipe(
      select(getOne)
    );
  }

  public handleChangeId(id): void {
    this.store.dispatch(new Actions.SelectPost(id));
  }

}

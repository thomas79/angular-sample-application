import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';

import { reducer as CounterReducer, CounterState } from 'src/app/counter/store/counter.reducer';
import { reducer as CommentsReducer, CommentsState } from 'src/app/comments/store/comments.reducer';

export interface State {
  counter: CounterState;
  comments: CommentsState;
}

export const reducers: ActionReducerMap<State> = {
  counter: CounterReducer,
  comments: CommentsReducer
};

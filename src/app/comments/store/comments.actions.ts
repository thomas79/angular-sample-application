import { Action } from '@ngrx/store';
import { Comment } from '../interfaces/comments.interfaces';

export enum Types {
  LOAD = '[Comments] Load',
  LOAD_SUCCESS = '[Comments] Load Success',
  LOAD_FAIL = '[Comments] Load Fail',
  SELECT_POST = '[Comments] Select Post'
}

export class Load implements Action {
  readonly type = Types.LOAD;
}

export class LoadSuccess implements Action {
  readonly type = Types.LOAD_SUCCESS;

  constructor(public payload: Comment[]) { }
}

export class LoadFail implements Action {
  readonly type = Types.LOAD_FAIL;
}

export class SelectPost implements Action {
  readonly type = Types.SELECT_POST;

  constructor(public payload: number) { }
}

export type Actions = Load
  | LoadSuccess
  | LoadFail
  | SelectPost;

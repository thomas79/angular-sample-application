import * as fromComments from './comments.reducer';
import * as Actions from './comments.actions';
import { Comment } from '../interfaces/comments.interfaces';

const { initialState, reducer } = fromComments;

const COMMENTS: Comment[] = [
  {
    id: 1,
    postId: 10,
    body: 'hello I am a test comment',
    name: 'cool dude',
    email: 'dude@cool.com'
  },
  {
    id: 2,
    postId: 10,
    body: 'hello I am another test comment',
    name: 'cooler dude',
    email: 'dude@cooler.com'
  }
];

const MORE_COMMENTS: Comment[] = [
  {
    id: 3,
    postId: 10,
    body: 'hello I am a test comment',
    name: 'cool dude',
    email: 'dude@cool.com'
  },
  {
    id: 4,
    postId: 10,
    body: 'hello I am another test comment',
    name: 'cooler dude',
    email: 'dude@cooler.com'
  }
];

const COMMENTS_STATE: fromComments.CommentsState = { ...initialState, comments: COMMENTS };
const LOADING_STATE: fromComments.CommentsState = { ...initialState, isLoading: true };
const LOADING_MORE_STATE: fromComments.CommentsState = { ...COMMENTS_STATE, isLoading: true };

describe('[Reducer] Comments', () => {
  let result: fromComments.CommentsState;

  describe('By default', () => {
    it('Returns the existing state', () => {
      expect(reducer(initialState, {} as any)).toEqual(initialState);
    });
  });

  describe('LOAD', () => {
    it('Sets isLoading to true', () => {
      result = reducer(initialState, new Actions.Load());
      expect(result.isLoading).toBeTruthy();
    });
  });

  describe('LOAD_SUCCESS', () => {
    describe('Initial load', () => {
      beforeEach(() => {
        result = reducer(LOADING_STATE, new Actions.LoadSuccess(COMMENTS));
      });

      it('Saves the loaded comments to the state', () => {
        expect(result).toEqual(COMMENTS_STATE);
      });

      it('Sets isLoading to false', () => {
        expect(result.isLoading).toBeFalsy();
      });
    });

    describe('Subsequent loading', () => {
      beforeEach(() => {
        result = reducer(LOADING_MORE_STATE, new Actions.LoadSuccess(MORE_COMMENTS));
      });

      it('Adds the new comments to the state', () => {
        expect(result.comments).toEqual(jasmine.arrayContaining(MORE_COMMENTS));
      });

      it('Does not overwrite the previous comments', () => {
        expect(result.comments).toEqual(jasmine.arrayContaining(COMMENTS));
      });
    });
  });

  describe('LOAD_FAIL', () => {
    describe('Initial loading', () => {
      it('Sets isLoading to false', () => {
        result = reducer(LOADING_STATE, new Actions.LoadFail());
        expect(result.isLoading).toBeFalsy();
      });
    });

    describe('Subsequent loading', () => {
      it('Does not modify the existing comments in state', () => {
        result = reducer(LOADING_MORE_STATE, new Actions.LoadFail());
        expect(result.comments).toEqual(COMMENTS);
      });
    });
  });

  describe('SELECT_POST', () => {
    describe('Updating the selected post ID', () => {
      it('Updates the ID for the selected post', () => {
        result = reducer(LOADING_STATE, new Actions.SelectPost(100));
        expect(result.selectedPost).toBe(100);
      });
    });
  });
});

import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

import { Comment } from '../../interfaces/comments.interfaces';

@Component({
  selector: 'app-comment-post',
  templateUrl: './comment-post.component.html',
  styleUrls: ['./comment-post.component.scss']
})
export class CommentPostComponent {

  @Input() comments: Observable<Comment[]>;

}

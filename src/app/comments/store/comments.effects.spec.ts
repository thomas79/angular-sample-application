import { TestBed } from '@angular/core/testing';
import { Subject, ReplaySubject, of, throwError } from 'rxjs';
import { provideMockActions } from '@ngrx/effects/testing';

import { CommentsEffects } from './comments.effects';
import { CommentsService } from '../services/comments.service';
import { Comment } from '../interfaces/comments.interfaces';

import * as commentsActions from './comments.actions';

const COMMENTS: Comment[] = [
  {
    id: 1,
    postId: 10,
    body: 'hello I am a test comment',
    name: 'cool dude',
    email: 'dude@cool.com'
  },
  {
    id: 2,
    postId: 10,
    body: 'hello I am another test comment',
    name: 'cooler dude',
    email: 'dude@cooler.com'
  }
];

describe('[Effects] Comments', () => {
  let effects: CommentsEffects;
  let actions$: Subject<any>;
  let mockHttp: jasmine.SpyObj<CommentsService>;

  beforeEach(() => {
    mockHttp = jasmine.createSpyObj('comments', ['getAll']);

    TestBed.configureTestingModule({
      providers: [
        CommentsEffects,
        provideMockActions(() => actions$),
        { provide: CommentsService, useValue: mockHttp }
      ]
    });

    effects = TestBed.get(CommentsEffects);
    actions$ = new ReplaySubject();
  });

  describe('Initialisation', () => {
    it('Initialises successfully', () => {
      expect(effects).toBeTruthy();
    });
  });

  describe('#load', () => {
    describe('Successful load', () => {
      it('Dispatches a LOAD_SUCCESS action with the fetched comments', () => {
        mockHttp.getAll.and.returnValue(
          of(COMMENTS)
        );

        actions$.next(new commentsActions.Load());

        effects.load$.subscribe((res) => {
          expect(res).toEqual(new commentsActions.LoadSuccess(COMMENTS));
        });
      });
    });

    describe('Unsuccessful load', () => {
      it('Dispatches a LOAD_FAIL action', () => {
        mockHttp.getAll.and.returnValue(
          throwError('something went wrong lol')
        );

        actions$.next(new commentsActions.Load());

        effects.load$.subscribe(res => {
          expect(res).toEqual(new commentsActions.LoadFail);
        });
      });
    });
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { CommentPostComponent } from './comment-post.component';
import { Wrapper } from 'src/utils/wrapper';
import { Comment } from '../../interfaces/comments.interfaces';

const COMMENTS: Comment[] = [
  {
    id: 1,
    postId: 10,
    body: 'hello I am a test comment',
    name: 'cool dude',
    email: 'dude@cool.com'
  },
  {
    id: 2,
    postId: 10,
    body: 'hello I am another test comment',
    name: 'cooler dude',
    email: 'dude@cooler.com'
  }
];

describe('[Component] CommentPost', () => {
  let component: CommentPostComponent;
  let fixture: ComponentFixture<CommentPostComponent>;
  let wrapper: Wrapper<CommentPostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentPostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture   = TestBed.createComponent(CommentPostComponent);
    component = fixture.componentInstance;
    wrapper   = new Wrapper(fixture);
  });

  describe('Initialisation', () => {
    it('Initialises successfully', () => {
      expect(component).toBeTruthy();
    });
  });

  describe('Rendering comments', () => {
    beforeEach(() => {
      component.comments = of(COMMENTS);
      fixture.detectChanges();
    });

    it('Renders an article for each comment passed in', () => {
      expect(wrapper.findAll('article').length)
        .toBe(COMMENTS.length);
    });

    it('Renders the name of the commenter', () => {
      expect(wrapper.findAll('article')[0].innerHTML)
        .toContain(COMMENTS[0].name);
    });

    it('Renders the body of the comment', () => {
      expect(wrapper.findAll('article')[0].innerHTML)
        .toContain(COMMENTS[0].body);
    });
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';

import { CounterComponent } from './counter.component';
import { Wrapper } from 'src/utils/wrapper';

const INITIAL_COUNTER_VALUE = 50;

describe('[Component] Counter', () => {
  let component: CounterComponent;
  let fixture: ComponentFixture<CounterComponent>;
  let wrapper: Wrapper<CounterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CounterComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture   = TestBed.createComponent(CounterComponent);
    component = fixture.componentInstance;
    wrapper   = new Wrapper(fixture);
  });

  describe('Initialisation', () => {
    it('Initialises successfully', () => {
      expect(component).toBeTruthy();
    });

    it('Renders the current counter value', () => {
      component.currentValue = of(INITIAL_COUNTER_VALUE);
      fixture.detectChanges();

      expect(wrapper.find('.counter').innerText)
        .toBe(INITIAL_COUNTER_VALUE.toString());
    });
  });

  describe('When the counter value changes', () => {
    it('Renders the updated counter value', () => {
      component.currentValue = of(INITIAL_COUNTER_VALUE);
      fixture.detectChanges();
      component.currentValue = of(INITIAL_COUNTER_VALUE * 2);
      fixture.detectChanges();

      expect(wrapper.find('.counter').innerText)
        .toBe((INITIAL_COUNTER_VALUE * 2).toString());
    });
  });
});

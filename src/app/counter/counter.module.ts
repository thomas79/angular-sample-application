import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';

import { reducer } from './store/counter.reducer';
import { CounterContainer } from './components/counter/counter.container';
import { CounterComponent } from './components/counter/counter.component';
import { CounterRoutingModule } from './counter-routing.module';

@NgModule({
  imports: [
    CommonModule,
    CounterRoutingModule,
    StoreModule.forFeature('counter', reducer)
  ],
  declarations: [
    CounterContainer,
    CounterComponent
  ],
  exports: [
    CounterContainer
  ]
})
export class CounterModule { }

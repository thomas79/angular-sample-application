import { CommentsState } from './comments.reducer';
import { createSelector, createFeatureSelector } from '@ngrx/store';

export const getCommentsState = createFeatureSelector<CommentsState>('comments');

export const getOne = createSelector(
  getCommentsState,
  (state: CommentsState) => state.comments.filter(comment => comment.postId === state.selectedPost)
);

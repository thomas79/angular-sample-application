import { ActionReducer } from '@ngrx/store';

import { Actions, Types } from './comments.actions';
import { Comment } from '../interfaces/comments.interfaces';

export interface CommentsState {
  selectedPost: number;
  isLoading: boolean;
  comments: Comment[];
}

export const initialState: CommentsState = {
  selectedPost: 1,
  isLoading: false,
  comments: []
};

export const reducer: ActionReducer<CommentsState> = (state: CommentsState = initialState, action: Actions) => {
  switch (action.type) {
    case Types.LOAD:
      return toggleLoad(state);

    case Types.LOAD_SUCCESS:
      return addComments(toggleLoad(state), action.payload);

    case Types.LOAD_FAIL:
      return toggleLoad(state);

    case Types.SELECT_POST:
      return selectPost(state, action.payload);

    default:
      return state;
  }
};

const toggleLoad = (state: CommentsState) => ({ ...state, isLoading: !state.isLoading });
const addComments = (state: CommentsState, payload: Comment[]) => ({ ...state, comments: [ ...state.comments, ...payload ] });
const selectPost = (state: CommentsState, payload: number) => ({ ...state, selectedPost: payload});

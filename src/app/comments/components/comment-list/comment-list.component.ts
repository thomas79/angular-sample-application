import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-comment-list-cmp',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.scss']
})
export class CommentListComponent {

  @Output() selectPostId: EventEmitter<number> = new EventEmitter();

  private DEFAULT_POST_ID = 1;

  public postId = this.DEFAULT_POST_ID;

  public onSubmitClick(): void {
    this.selectPostId.emit(this.postId);
  }

}
